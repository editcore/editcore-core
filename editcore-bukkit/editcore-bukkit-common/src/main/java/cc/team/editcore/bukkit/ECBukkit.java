package cc.team.editcore.bukkit;

import cc.team.editcore.EditCore;
import cc.team.editcore.EditcorePlugin;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

/**
 * Bukkit imlementation for Editcore
 */
public class ECBukkit {

    private final WorldEditPlugin worldEditPlugin;
    private final EditcorePlugin ecPlugin;

    public ECBukkit(WorldEditPlugin worldEditPlugin) {
        this.worldEditPlugin = worldEditPlugin;
        this.ecPlugin = EditcorePlugin.getInstance();
    }

    /**
     * Initialize editcore for bukkit
     */
    public void init() {
        ECPlatformBukkit ecPlatformBukkit = new ECPlatformBukkit(ecPlugin);
        EditCore.init(ecPlatformBukkit);
    }
}
