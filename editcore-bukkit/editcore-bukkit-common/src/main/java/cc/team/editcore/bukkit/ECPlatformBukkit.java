package cc.team.editcore.bukkit;

import cc.team.editcore.ECPlatform;
import cc.team.editcore.ECPlayer;
import cc.team.editcore.EditcorePlugin;
import cc.team.editcore.network.PacketPipeline;
import cc.team.editcore.network.PacketPipelineAdapter;
import com.sk89q.worldedit.util.command.SimpleDispatcher;
import com.sk89q.worldedit.util.command.parametric.ParametricBuilder;

/**
 * Platform for bukkit
 */
public class ECPlatformBukkit implements ECPlatform {

    private final EditcorePlugin ecPlugin;
    private final BukkitPacketPipelineAdapter pipelineAdapter;

    public ECPlatformBukkit(EditcorePlugin ecPlugin) {
        this.ecPlugin = ecPlugin;
        this.pipelineAdapter = new BukkitPacketPipelineAdapter(ecPlugin);
    }

    @Override
    public void registerCommands(SimpleDispatcher dispatcher, ParametricBuilder builder) {
        this.ecPlugin.getModulesManager().registerCommands(dispatcher, builder);
    }

    @Override
    public PacketPipelineAdapter getPinelineAdapter() {
        return pipelineAdapter;
    }

    //TODO fix maven to use ECPlayer
    @Override
    public void onPlayerAuth(ECPlayer player) {
        this.ecPlugin.getModulesManager().onPlayerAuth(player.getPlayer());
    }
}
