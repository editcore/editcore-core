package cc.team.editcore.bukkit;

import cc.team.editcore.ECPlayer;
import cc.team.editcore.EditcorePlugin;
import cc.team.editcore.network.AbstractPacket;
import cc.team.editcore.network.PacketPipelineAdapter;
import cc.team.editcore.network.PacketPipeline;
import com.boydti.fawe.FaweAPI;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

/**
 * PacketPipelineAdapter for Bukkit
 */
public class BukkitPacketPipelineAdapter implements PacketPipelineAdapter, PluginMessageListener {

    private PacketPipeline pipeline;
    private EditcorePlugin ecPlugin;

    public BukkitPacketPipelineAdapter(EditcorePlugin plugin){
        this.ecPlugin = plugin;
    }

    @Override
    public void register(PacketPipeline packetPipeline) {
        this.pipeline = packetPipeline;
    }

    @Override
    public void sendPacketTo(AbstractPacket packet, com.sk89q.worldedit.entity.Player player) throws UnsupportedOperationException {
        this.pipeline.sendPacketToPlayer(packet, player);
    }

    @Override
    public void sendPacketTo(com.sk89q.worldedit.entity.Player p, byte[] data) {
        this.ecPlugin.sendPacketTo(BukkitAdapter.adapt(p), PacketPipeline.CHANNEL, data);
    }

    @Override
    public void sendPacketTo(AbstractPacket packet) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sendPacketToServer(byte[] data) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] data) {
        this.pipeline.onMessageReceived(channel, new ECPlayer(FaweAPI.wrapPlayer(player)), data, false);
    }
}
