package cc.team.editcore.message;

import cc.team.editcore.EditcorePlugin;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Logger for Editcore
 */
public class ECLogger {
    private static Logger logger = Logger.getLogger("[Editcore]");

    /**
     * Basic message, information about what's going on
     * @param msg (String) : Message
     */
    public static void log(String msg){
        logger.log(Level.INFO, msg);
    }

    /**
     * Warning message, might be a problem
     * @param msg (String) : Message
     */
    public static void warn(String msg){
        logger.log(Level.WARNING, msg);
    }

    /**
     * Error message, there is a problem
     * @param msg (String) : Message
     */
    public static void error(String msg){
        logger.log(Level.SEVERE, msg);
    }

    /**
     * Error message, there is a problem
     * Prints the error linked
     * @param msg (String) : Message
     * @param error (Throwable) : Error to print
     */
    public static void error(String msg, Throwable error){
        logger.log(Level.SEVERE, msg, error);
    }

    /**
     * Debug message
     * @param msg (String) : Message
     */
    public static void debug(String msg){
        if(EditcorePlugin.DEBUG)
            logger.log(Level.INFO, msg);
    }

    /**
     * Print a message with custom level
     * @param msg (String) : Message
     * @param level (Level) : Level of the message
     */
    public static void log(String msg, Level level){
        logger.log(level, msg);
    }
}
