package cc.team.editcore;

import cc.team.editcore.configuration.Configuration;
import cc.team.editcore.utils.PluginDownloader;
import cc.team.editcore.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.File;

/**
 * Main Editcore plugin class. Loads everything
 */
public class EditcorePlugin extends JavaPlugin {

    private static EditcorePlugin INSTANCE;
    public static final boolean DEBUG = true;

    private ModulesManager modulesManager;
    //TODO
    private Configuration config;

    public static EditcorePlugin getInstance() {
        return INSTANCE;
    }

    @Override
    public void onLoad(){
        INSTANCE = this;

        config = new Configuration();

        //Initialize modules
        modulesManager = new ModulesManager(this);
        modulesManager.registerModules(getServer().getPluginManager());
    }

    @Override
    public void onEnable() {
        Plugin worldedit = Utils.getPluginFromName("Worldedit");
        if(worldedit == null){
            //TODO ECMessage
            try {
                File f = PluginDownloader.downloadWorldedit();
                getServer().getPluginManager().loadPlugin(f);
            } catch (InvalidPluginException e) {
                e.printStackTrace();
            } catch (InvalidDescriptionException e) {
                e.printStackTrace();
            }
        }

        modulesManager.checkForUpdates();

        //Enable phases
        modulesManager.preWorldEditEnable();

        getPluginLoader().enablePlugin(worldedit);

        modulesManager.enableModules(getServer().getPluginManager());
    }

    /**
     * @return EditCore's configuration
     */
    public Configuration getConfiguration() {
        return config;
    }

    /**
     * The modules manager of editcore
     * @return (ModulesManager)
     */
    public ModulesManager getModulesManager() {
        return modulesManager;
    }

    /**
     * Initialize packet pipeline. Give the channel it will take and the object that will receive incoming packets
     * @param channel (String) : channel
     * @param receiver (PluginMessageListener) : Packet receiver
     */
    public void initPacketPipeline(String channel, PluginMessageListener receiver){
        getServer().getMessenger().registerIncomingPluginChannel(this, channel, receiver);
        getServer().getMessenger().registerOutgoingPluginChannel(this, channel);
    }

    /**
     * Send a package to given player
     * @param p (Player) : Player to send to
     * @param channel (String) : Channel
     * @param data (byte[]) : data of the package
     */
    public void sendPacketTo(Player p, String channel, byte[] data) {
        p.sendPluginMessage(this, channel, data);
    }
}
