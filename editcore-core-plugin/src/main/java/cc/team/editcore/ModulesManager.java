package cc.team.editcore;

import cc.team.editcore.module.Module;
import com.google.common.collect.Sets;
import com.sk89q.worldedit.entity.Player;
import com.sk89q.worldedit.util.command.SimpleDispatcher;
import com.sk89q.worldedit.util.command.parametric.ParametricBuilder;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import java.util.*;

/**
 * Manage loaded modules, used to call their own methods.
 */
public class ModulesManager {

    /**
     * A complete list of all available modules
     */
    public static final Set<String> availableModules = Collections.unmodifiableSet(Sets.newHashSet(
    ));

    private final EditcorePlugin editcore;
    private Map<String, Module> loadedModules;

    /**
     * Creates a ModuleManager
     * @param editcorePlugin
     */
    public ModulesManager(EditcorePlugin editcorePlugin) {
        this.editcore = editcorePlugin;
    }

    /**
     * Check which modules are loaded
     * @param pm (PluginManager) : Bukkit's plugin manager
     * @return false if there was an error
     */
    public boolean registerModules(PluginManager pm) {
        loadedModules = new HashMap();

        for(String moduleName : availableModules){
            Plugin module = pm.getPlugin(moduleName);
            if(module != null){
                if(!Module.class.isInstance(module)){
                    //TODO
                    return false;
                }else {
                    //TODO Add module enable
                    loadedModules.put(moduleName, (Module) module);
                }
            }
        }

        return true;
    }

    /**
     * Dispatch preEnable() to modules
     */
    public void preWorldEditEnable() {
        for(Module module : loadedModules.values()){
            module.preWorldeditEnable(editcore);
        }
    }

    /**
     * Enable all modules
     * @param pluginManager (PluginManager) : Bukkit's plugin manager
     */
    public void enableModules(PluginManager pluginManager) {
        for(Module module : loadedModules.values()){
            pluginManager.enablePlugin(module);
        }
    }

    /**
     * Dispatch registerCommands() to the modules
     * @param dispatcher (SimpleDispatcher) : WorldEdit's Command Dispatcher
     * @param builder (ParametricBuilder)
     */
    public void registerCommands(SimpleDispatcher dispatcher, ParametricBuilder builder) {
        for(Module module : loadedModules.values()){
            module.registerCommands(dispatcher, builder);
        }
    }

    public void checkForUpdates() {
    }

    public void onPlayerAuth(Player player) {
    }
}
