package cc.team.editcore.module;

import cc.team.editcore.EditcorePlugin;
import com.sk89q.worldedit.util.command.SimpleDispatcher;
import com.sk89q.worldedit.util.command.parametric.ParametricBuilder;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Module of EditCore
 */
public class Module extends JavaPlugin {

    /**
     * Called before worledit enables
     * @param editcore (EditcorePlugin) : Main plugin
     */
    public void preWorldeditEnable(EditcorePlugin editcore){}

    /**
     * Allows module to register commands into the dispatcher
     * @param dispatcher
     * @param builder
     */
    public void registerCommands(SimpleDispatcher dispatcher, ParametricBuilder builder){}
}
