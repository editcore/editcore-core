package cc.team.editcore.utils;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

/**
 *Utility class
 */
public class Utils {

    /**
     * Get a plugin from its name
     * @param name (String) : Nae of the plugin
     * @return (Plugin) : The plugin
     */
    public static Plugin getPluginFromName(String name){
        return Bukkit.getPluginManager().getPlugin(name);
    }
}
