package cc.team.editcore.network.cloud;

import cc.team.editcore.network.ECInputStream;
import cc.team.editcore.network.ECOutputStream;
import cc.team.editcore.network.Sendable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

//TODO throws exception

public class CloudNetworkManager {

    private final NodeJsNetworker networker;
    private final Map<Short, Class<? extends Sendable>> registry = new HashMap<>();

    public CloudNetworkManager() throws IOException {
        networker = new NodeJsNetworker();
    }

    public void register(short id, Class<? extends Sendable> bufferableClass){
        if(registry.containsKey(id))
            throw new IllegalArgumentException("Cannot register bufferable" + bufferableClass + " because the id " + id + " is already taken by " + registry.get(id));
        if(registry.containsValue(bufferableClass))
            throw new IllegalArgumentException("The class " + bufferableClass + " is already registered as a bufferable");

        registry.put(id, bufferableClass);
    }

    public void send(Sendable sendable) throws IOException {
        short id = findID(sendable);
        ECOutputStream stream = new ECOutputStream(new ByteArrayOutputStream());

        stream.writeShort(id);
        sendable.encodeInto(stream);
        networker.sendData(stream.toByteArray());
    }

    public <T extends Sendable> T get(URL url){
        byte[] bytes = networker.getData(url);
        ECInputStream buffer = new ECInputStream(new ByteArrayInputStream(bytes));
        try{
            Short id = buffer.readShort();
            Class<? extends Sendable> clazz = registry.get(id);
            //noinspection unchecked
            T bufferable = (T) clazz.newInstance();
            bufferable.decodeFrom(buffer);
            return bufferable;
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Short findID(Sendable buffer){
        for(Map.Entry<Short, Class<? extends Sendable>> entry : registry.entrySet()){
            if(buffer.getClass().equals(entry.getValue()))
                return entry.getKey();
        }
        throw new IllegalArgumentException("The bufferable " + buffer.getClass() + " is not registered");
    }
}
