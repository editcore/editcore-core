package cc.team.editcore.network;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A variable length key, can be converted to Hex code
 */
public final class ECKey {

    private final byte[] code;

    /**
     * Instantiate the key with the bits
     * @param code (byte[])
     */
    public ECKey(byte[] code) {
        this.code = code;
    }

    /**
     * Instantiate the key with the String hex code
     * @param code (String)
     */
    public ECKey(String code) {
        this.code = Symbol.toBytes(Symbol.of(code));
    }

    /**
     * @return the bits of the key
     */
    public byte[] getCode(){
        return code;
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(Symbol s : Symbol.of(code))
            builder.append(s.symbol);
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return getClass().isInstance(obj) && Arrays.equals(((ECKey) obj).code, this.code);
    }

    //TODO can be done better, for sure
    private enum Symbol {
        S0('0'),
        S1('1'),
        S2('2'),
        S3('3'),
        S4('4'),
        S5('5'),
        S6('6'),
        S7('7'),
        S8('8'),
        S9('9'),
        SA('A'),
        SB('B'),
        SC('C'),
        SD('D'),
        SE('E'),
        SF('F');

        private static List<Symbol> SYMBOLS = Collections.unmodifiableList(Arrays.asList(values()));

        private char symbol;

        Symbol(char symbol) {
            this.symbol = symbol;
        }

        static Symbol of(char c){
            int index = c > S9.symbol ? c - SA.symbol + SA.ordinal() : c - S0.symbol;
            if(index < 0 || index >= 16)
                throw new IllegalArgumentException();
            return SYMBOLS.get(index);
        }

        static Symbol[] of(String code){
            Symbol[] symbols = new Symbol[code.length()];
            for(int i = 0; i < code.length(); ++i){
                symbols[i] = of(code.charAt(i));
            }

            return symbols;
        }

        static Symbol[] of(byte[] code){
            Symbol[] symbols = new Symbol[code.length * 2 - (code[code.length - 1] < 16 ? 1 : 0)];
            for(int i = 0; i < code.length; ++i){
                byte b = code[i];
                Symbol s1 = SYMBOLS.get(b & 0b00001111);
                Symbol s2 = SYMBOLS.get((b & 0b11110000) >> 4);
                symbols[i*2] = s1;
                if(i*2+1 < symbols.length)
                    symbols[i*2+1] = s2;
            }
            return symbols;
        }

        static byte[] toBytes(Symbol[] symbols){
            int size = symbols.length + (symbols.length % 2 == 0 ? 0 : 1);
            byte[] bytes = new byte[size];
            for(int i = 0; i < symbols.length; ++i){
                if(i+1 < symbols.length){
                    bytes[i/2] = (byte) (symbols[i].ordinal() | (symbols[i+1].ordinal() << 4));
                    ++i;
                }else{
                    bytes[i] = (byte) symbols[i].ordinal();
                }
            }
            return bytes;
        }
    }
}
