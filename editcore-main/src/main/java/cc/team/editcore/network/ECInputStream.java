package cc.team.editcore.network;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.BlockVector2D;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.Vector2D;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class ECInputStream extends InputStream {

    private final InputStream stream;

    public ECInputStream(InputStream stream) {
        this.stream = stream;
    }

    @Override
    public int read() throws IOException {
        return stream.read();
    }

    /**
     * Read a long
     * @return (long)
     */
    public long readLong() throws IOException {
        return ((long)read() << 56) | ((long)read() << 48) | ((long)read() << 40) | ((long)read() << 32) | ((long)read() << 24) | ((long)read() << 16) | ((long)read() << 8) | (long)read();
    }

    /**
     * Read an int
     * @return (int) read int
     */
    public int readInt() throws IOException {
        return (read() << 24) | (read() << 16) | (read() << 8) | read();
    }
    
    /**
     * Read an int
     * @return (int) read int
     */
    public int readSmallInt() throws IOException {
        return (read() << 16) | (read() << 8) | read();
    }

    /**
     * Read a short
     * @return (short)
     */
    public short readShort() throws IOException {
        return (short) ((read() << 8) | read());
    }

    /**
     * Read a short
     * @return (short)
     */
    public byte readByte() throws IOException {
        return (byte) (read());
    }

    /**
     * Read a double
     * @return (double)
     */
    public double readDouble() throws IOException {
        return Double.longBitsToDouble(readLong());
    }

    /**
     * Read a float
     * @return (float)
     */
    public float readFloat() throws IOException {
        return Float.intBitsToFloat(readInt());
    }

    /**
     * Read a String
     * @return (String)
     */
    public String readString() throws IOException {
        return new String(readBytes(), Charset.forName("UTF-8"));
    }

    /**
     * Read a Vector
     * @return (Vector)
     */
    public Vector readVector() throws IOException {
        double x = readDouble();
        double y = readDouble();
        double z = readDouble();
        return new Vector(x, y, z);
    }

    /**
     * Read a Vector2D
     * @return (Vector2D)
     */
    public Vector2D readVector2D() throws IOException {
        double x = readDouble();
        double z = readDouble();
        return new Vector2D(x, z);
    }

    /**
     * Read a BlockVector
     * @return (BlockVector)
     */
    public BlockVector readBlockVector() throws IOException {
        double x = readInt();
        double y = readInt();
        double z = readInt();
        return new BlockVector(x, y, z);
    }

    /**
     * Read a BlockVector2D
     * @return (BlockVector2D)
     */
    public BlockVector2D readBlockVector2D() throws IOException {
        double x = readInt();
        double z = readInt();
        return new BlockVector2D(x, z);
    }

    /**
     * Read a byte array with saved length
     * @return (byte) read byte[]
     */
    public byte[] readBytes() throws IOException {
        int length = readInt();
        return readBytes(length);
    }

    /**
     * Read a byte array with specified length
     * @return (byte) read byte[]
     */
    public byte[] readBytes(int length) throws IOException {
        byte[] bytes = new byte[length];
        for(int i = 0; i < length; ++i){
            bytes[i] = (byte) read();
        }
        return bytes;
    }

    /**
     * Read a double array with saved length
     * @return (double[])
     */
    public double[] readDoubles() throws IOException {
        int length = readInt();
        return readDoubles(length);
    }

    /**
     * Read a byte array with given length
     * @param length The of the array
     * @return (double) read double[]
     */
    public double[] readDoubles(int length) throws IOException {
        double[] doubles = new double[length];
        for(int i = 0; i < length; ++i){
            doubles[i] = read();
        }
        return doubles;
    }

    /**
     * Read an ECKey from the stream
     * @param length the fixed length of the key
     * @return (ECKey)
     */
    public ECKey readKey(int length) throws IOException {
        return new ECKey(readBytes(length));
    }

    /**
     * Read a List of elements
     * @param supplier (ListSupplierReading<T>) Gives the rule to read an element
     * @param <T> (Elements being extracted)
     * @return (List<T>)
     */
    public <T> List<T> readList(ListSupplierReading<T> supplier) throws IOException {
        int size = readInt();
        List<T> list = new ArrayList<>();
        for(int i = 0; i < size; ++i)
            list.add(supplier.run(this));

        return list;
    }

    public interface ListSupplierReading<T>{
        T run(ECInputStream stream) throws IOException;
    }
}
