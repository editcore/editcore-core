package cc.team.editcore.network.packet;

import cc.team.editcore.ECPlayer;
import cc.team.editcore.EditCore;
import cc.team.editcore.network.AbstractPacket;
import cc.team.editcore.network.ECInputStream;
import cc.team.editcore.network.ECKey;
import cc.team.editcore.network.ECOutputStream;

import java.io.IOException;

/**
 * The packet is sent by the server when a player connects or the server is reloaded.
 * It gives him its key that the player sends to the cloud. It will ensure that the server is the only one who have access to the player's data.
 */
public class ServerAuthPacket extends AbstractPacket {

    private ECKey serverKey;

    @Override
    public Type getPacketType() {
        return Type.CLIENT;
    }

    @Override
    public void handleClient(ECPlayer player, EditCore editCore) {
        //TODO register the server as current server for the player
        editCore.getPlatform().getPinelineAdapter().sendPacketTo(new PlayerAuthPacket(player));
    }

    @Override
    public void handleServer(ECPlayer player, EditCore editCore) {}

    @Override
    public void encodeInto(ECOutputStream stream) throws IOException {

    }

    @Override
    public void decodeFrom(ECInputStream buf) throws IOException {

    }
}
