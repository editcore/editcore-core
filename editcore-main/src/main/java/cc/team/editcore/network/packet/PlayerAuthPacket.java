package cc.team.editcore.network.packet;

import cc.team.editcore.ECPlayer;
import cc.team.editcore.EditCore;
import cc.team.editcore.network.*;

import java.io.IOException;

/**
 * The player sends this to the server to give him its session key
 */
public class PlayerAuthPacket extends AbstractPacket {

    private ECKey sessionKey;

    public PlayerAuthPacket(){}

    public PlayerAuthPacket(ECPlayer player) {
        this.sessionKey = player.getSessionKey();
    }

    @Override
    public Type getPacketType() {
        return Type.SERVER;
    }

    @Override
    public void handleClient(ECPlayer player, EditCore editCore) {}

    @Override
    public void handleServer(ECPlayer player, EditCore editCore) {
        editCore.getPlatform().onPlayerAuth(player);
    }

    @Override
    public void encodeInto(ECOutputStream stream) throws IOException {
        stream.writeKey(sessionKey);
    }

    @Override
    public void decodeFrom(ECInputStream stream) throws IOException {
        this.sessionKey = stream.readKey(ECPlayer.SESSION_KEY_SIZE);
    }
}
