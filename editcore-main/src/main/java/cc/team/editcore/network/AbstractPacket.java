package cc.team.editcore.network;

import cc.team.editcore.ECPlayer;
import cc.team.editcore.EditCore;

/**
 * Skeleton of a packet
 */
public abstract class AbstractPacket implements ECSerializable {

    /**
     * Get the packet handling type
     * @return (AbstractPacket.Type)
     */
    public abstract Type getPacketType();

    /**
     * Execut the packet client side
     * @param player (Player) : Current player
     * @param editCore (Editcore) : current Editcore
     */
    public abstract void handleClient(ECPlayer player, EditCore editCore);

    /**
     * Execut the packet server side
     * @param player (Player) : Current player
     * @param editCore (Editcore) : current Editcore
     */
    public abstract void handleServer(ECPlayer player, EditCore editCore);

    /**
     * Type of handling of a packet
     */
    public enum Type {
        SERVER(false, true),
        CLIENT(true, false),
        BIDIRECTIONAL(true, true);

        private boolean server, client;

        Type(boolean c, boolean s) {
            server = s;
            client = c;
        }

        public boolean isServer() {
            return server;
        }

        public boolean isClient() {
            return client;
        }
    }
}

