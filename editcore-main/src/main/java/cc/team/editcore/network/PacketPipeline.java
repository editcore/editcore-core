package cc.team.editcore.network;

import cc.team.editcore.ECLogger;
import cc.team.editcore.ECPlatform;
import cc.team.editcore.ECPlayer;
import cc.team.editcore.EditCore;
import com.sk89q.worldedit.entity.Player;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles packet sending, receiving and handling
 */
public class PacketPipeline {

    public static final String CHANNEL = "EDITCORE";

    //List of all different packets
    private EditCore editcore;
    private ECPlatform platform;

    private final Map<Short, Class<? extends AbstractPacket>> registry = new HashMap<>();

    /**
     * Register a packet with the given id
     * @param id
     * @param packetClass
     */
    public void register(short id, Class<? extends AbstractPacket> packetClass){
        if(registry.containsKey(id))
            throw new IllegalArgumentException("Cannot register packet " + packetClass + " because the id " + id + " is already taken by " + registry.get(id));
        if(registry.containsValue(packetClass))
            throw new IllegalArgumentException("The class " + packetClass + " is already registered as a packet");

        registry.put(id, packetClass);
    }

    /**
     * Initialize the pipeline
     * @param editcore (EditCore) : Used to initialize
     * @param platform (ECPlatform) : Used to initialize
     */
    public void init(EditCore editcore, ECPlatform platform){
        this.editcore = editcore;
        this.platform = platform;
    }

    /**
     * Send a packet to the server (from the client)
     * @param packet (AbstractPacket) the packet to send
     */
    public void sendPacket(AbstractPacket packet){
        try {
            platform.getPinelineAdapter().sendPacketToServer(preparePacket(packet));
        } catch (IOException e) {
            //TODO Send message to user
            e.printStackTrace();
        }
    }

    /**
     * Send a packet to the player (from the server)
     * @param packet (AbstactPacket) the packet to send
     * @param p (Player) the player to send to
     */
    public void sendPacketToPlayer(AbstractPacket packet, Player p){
        try {
            platform.getPinelineAdapter().sendPacketTo(p, preparePacket(packet));
            //TODO Send message to user
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert a packet to byte[] to be able to send it
     * @param packet (AbstractPacket) : the packet to convert
     * @return (byte[]) the converted bytes
     */
    private byte[] preparePacket(AbstractPacket packet) throws IOException {
        ECOutputStream stream = new ECOutputStream(new ByteArrayOutputStream());
        short id =  findID(packet);

        stream.writeShort(id);
        packet.encodeInto(stream);
        return stream.toByteArray();
    }

    /**
     * Called when a packet is received
     * @param channel (String) : channel
     * @param p (Player) : The player, the sender for the server side, and the main player for the client side
     * @param data (byte[]) : The data of the packet
     * @param fromServer (boolean) : true if the packet was sent from the server
     */
    public void onMessageReceived(String channel, ECPlayer p, byte[] data, boolean fromServer) {
        ECInputStream stream = new ECInputStream(new ByteArrayInputStream(data));

        try {
            short id = stream.readShort();

            Class<? extends AbstractPacket> clazz = registry.get(id);
            AbstractPacket packet = clazz.newInstance();
            packet.decodeFrom(stream);

            if(packet.getPacketType().isServer() && !fromServer)
                packet.handleServer(p, editcore);
            else if(packet.getPacketType().isClient() && fromServer)
                packet.handleClient(p, editcore);
            else
                ECLogger.warn("Unhandled packet : " + packet.getClass());

        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            //TODO Send message to user
            e.printStackTrace();
        }
    }

    private Short findID(AbstractPacket packet){
        for(Map.Entry<Short, Class<? extends AbstractPacket>> entry : registry.entrySet()){
            if(packet.getClass().equals(entry.getValue()))
                return entry.getKey();
        }
        throw new IllegalArgumentException("The packet class " + packet.getClass() + " is not registered");
    }
}
