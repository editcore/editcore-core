package cc.team.editcore.network;

import com.sk89q.worldedit.entity.Player;

/**
 * Adapter for the Packet Pipeline
 */
public interface PacketPipelineAdapter {

    /**
     * Register the PacketPipeline to the adapter
     * @param pipeline (PacketPipeline)
     */
    void register(PacketPipeline pipeline);

    /**
     * Send a packet to the targeted player
     * @param packet (AbtractPacket) : Packet to send
     * @param player (Player) : Target
     * @throws UnsupportedOperationException if the platform is only client side
     */
    void sendPacketTo(AbstractPacket packet, Player player) throws UnsupportedOperationException;

    /**
     * Send a packet to the targeted player
     * @param player (Player) : Target
     * @param data (byte[]) : data to send
     * @throws UnsupportedOperationException if the platform is only client side
     */
    void sendPacketTo(Player player, byte[] data);

    /**
     * Send a packet to the server
     * @param packet (AbtractPacket) : Packet to send
     * @throws UnsupportedOperationException if the platform is only server side
     */
    void sendPacketTo(AbstractPacket packet) throws UnsupportedOperationException;

    /**
     * Send a packet to the server
     * @param data (byte[]) : data to send
     * @throws UnsupportedOperationException if the platform is only server side
     */
    void sendPacketToServer(byte[] data) throws UnsupportedOperationException;
}
