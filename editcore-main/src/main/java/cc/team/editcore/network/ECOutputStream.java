package cc.team.editcore.network;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.BlockVector2D;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.Vector2D;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.List;

public class ECOutputStream extends OutputStream {

    private final OutputStream stream;

    public ECOutputStream(OutputStream stream) {
        this.stream = stream;
    }

    @Override
    public void write(int b) throws IOException {
        stream.write(b);
    }

    /**
     * Write a long (8 bytes)
     * @param value (long)
     */
    public void writeLong(long value) throws IOException {
        write((byte)(value >> 56));
        write((byte)(value >> 48));
        write((byte)(value >> 40));
        write((byte)(value >> 32));
        write((byte)(value >> 24));
        write((byte)(value >> 16));
        write((byte)(value >> 8));
        write((byte)value);
    }

    /**
     * Write an int (4 bytes)
     * @param value (int)
     */
    public void writeInt(int value) throws IOException {
        write((byte)(value >> 24));
        write((byte)(value >> 16));
        write((byte)(value >> 8));
        write((byte)value);
    }

    /**
     * Write a small int (3 bytes)
     * @param value (int)
     */
    public void writeSmallInt(int value) throws IOException {
        write((byte)(value >> 16));
        write((byte)(value >> 8));
        write((byte)value);
    }

    /**
     * Write a small int (3 bytes)
     * @param value (int)
     */
    public void writeShort(short value) throws IOException {
        write((byte)(value >> 8));
        write((byte)value);
    }

    /**
     * Write a byte
     * @param value (byte)
     */
    public void writeByte(byte value) throws IOException {
        write(value);
    }

    /**
     * Write a float
     * @param value (float)
     */
    public void writeFloat(float value) throws IOException {
        writeInt(Float.floatToIntBits(value));
    }

    /**
     * Write a double
     * @param value (double)
     */
    public void writeDouble(double value) throws IOException {
        writeLong(Double.doubleToLongBits(value));
    }

    /**
     * Write byte array and save its length
     * @param bytes : bytes to write
     */
    public void writeBytes(byte[] bytes) throws IOException{
        writeBytes(bytes, true);
    }

    /**
     * Write a byte array
     * @param bytes (byte[]) data
     * @param notifyLength true if the length must be saved
     */
    public void writeBytes(byte[] bytes, boolean notifyLength) throws IOException {
        if(notifyLength)
            writeInt(bytes.length);
        write(bytes);
    }

    /**
     * Write double array and save its length
     * @param doubles : doubles to write
     */
    public void writeDoubles(double[] doubles) throws IOException {
        writeDoubles(doubles, true);
    }

    /**
     * Write a double array
     * @param doubles (double[]) data
     * @param notifyLength true if the length must be saved
     */
    public void writeDoubles(double[] doubles, boolean notifyLength) throws IOException {
        if(notifyLength)
            writeInt(doubles.length);
        for(double bit : doubles){
            writeDouble(bit);
        }
    }
    /**
     * Write a String
     * @param value (String)
     */
    public void writeString(String value) throws IOException {
        writeBytes(value.getBytes(Charset.forName("UTF-8")));
    }

    /**
     * Write a Vector
     * @param vec (Vector)
     */
    public void writeVector(Vector vec) throws IOException {
        writeDouble(vec.getX());
        writeDouble(vec.getY());
        writeDouble(vec.getZ());
    }

    /**
     * Write a Vector2D
     * @param vec (Vector2D)
     */
    public void writeVector2D(Vector2D vec) throws IOException {
        writeDouble(vec.getX());
        writeDouble(vec.getZ());
    }

    /**
     * Write a BlockVector
     * @param vec (BlockVector)
     */
    public void writeBlockVector(BlockVector vec) throws IOException {
        writeInt(vec.getBlockX());
        writeInt(vec.getBlockY());
        writeInt(vec.getBlockZ());
    }

    /**
     * Write a BlockVector2D
     * @param vec (BlockVector2D)
     */
    public void writeBlockVector2D(BlockVector2D vec) throws IOException {
        writeInt(vec.getBlockX());
        writeInt(vec.getBlockZ());
    }

    public void writeKey(ECKey id) throws IOException {
        write(id.getCode());
    }

    /**
     * Write a list of elements in the converter
     * @param <T> Tpe of the elements
     * @param list (List<T>) List containing the elements
     * @param supplier (ListSupplierWriting<T>) Supplier that gives the rule to convert T from
     */
    public <T> void writeList(List<T> list, ListSupplierWriting<T> supplier) throws IOException {
        this.writeInt(list.size());
        for (T t : list)
            supplier.run(t, this);
    }

    /**
     * A simplified version of writeList() only for ECSerializable
     * @param list (List<T>) Elements to write
     * @param <T> (Type of the elements, must extends ECSerializable)
     */
    public <T extends ECSerializable> void writeList(List<T> list) throws IOException {
        writeList(list, ECSerializable::encodeInto);
    }

    public byte[] toByteArray() {
        if(stream instanceof ByteArrayOutputStream)
            return ((ByteArrayOutputStream)stream).toByteArray();
        else
            throw new UnsupportedOperationException();
    }

    public interface ListSupplierWriting<T>{
        void run(T type, ECOutputStream stream) throws IOException;
    }
}
