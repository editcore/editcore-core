package cc.team.editcore.network.cloud;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;

public class NodeJsNetworker {

    private final String IP = "";
    private final int PORT = 80;
    private final Socket socket;

    NodeJsNetworker() throws IOException {
        this.socket = new Socket(IP, PORT);
    }

    public void sendData(byte[] data) throws IOException {
        this.socket.getOutputStream().write(data);
    }

    public byte[] getData(URL url) {
        return null;
    }

}
