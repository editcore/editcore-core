package cc.team.editcore.network;

import java.io.IOException;

public interface ECSerializable {

    /**
     * Encode the packet into the buffer
     * @param stream (ECOutputStream)
     */
    void encodeInto(ECOutputStream stream) throws IOException;

    /**
     * Decode the packet from the buffer
     * @param buf (ByteBuffer)
     */
    void decodeFrom(ECInputStream buf) throws IOException;
}
