package cc.team.editcore.network;

/**
 * A ECSerializable that can be sent
 */
public interface Sendable extends ECSerializable {

}
