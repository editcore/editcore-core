package cc.team.editcore;

import cc.team.editcore.network.PacketPipeline;
import cc.team.editcore.network.PacketPipelineAdapter;
import com.sk89q.worldedit.entity.Player;
import com.sk89q.worldedit.util.command.SimpleDispatcher;
import com.sk89q.worldedit.util.command.parametric.ParametricBuilder;

/**
 * Interface to handle platform dependent actions
 */
public interface ECPlatform {

    /**
     * Called during the command registration process. Allow other
     * @param dispatcher
     * @param builder
     */
    void registerCommands(SimpleDispatcher dispatcher, ParametricBuilder builder);

    /**
     * @return the PacketPipelineAdapter of the current Platform
     */
    PacketPipelineAdapter getPinelineAdapter();

    /**
     * Called when a player authenticate itself on a server
     * @param player
     */
    void onPlayerAuth(ECPlayer player);
}
