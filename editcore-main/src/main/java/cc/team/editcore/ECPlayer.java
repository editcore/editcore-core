package cc.team.editcore;

import cc.team.editcore.network.ECKey;
import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.object.FawePlayer;
import com.sk89q.worldedit.entity.Player;

/**
 * Player instance for Editcore. Store the authentication key and process
 */
public class ECPlayer {

    //TODO choose a number
    public static final int SESSION_KEY_SIZE = 128;

    private final FawePlayer player;
    /** Player current's auth key works only on this server */
    private ECKey authKey;

    public ECPlayer(FawePlayer player) {
        this.player = player;
    }

    public ECPlayer(Player player) {
        this.player = FaweAPI.wrapPlayer(player);
    }

    public ECKey getSessionKey() {
        return null;
    }

    public Player getPlayer() {
        return this.player.toWorldEditPlayer();
    }
}
