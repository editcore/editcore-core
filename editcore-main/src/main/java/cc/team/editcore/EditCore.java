package cc.team.editcore;

import cc.team.editcore.network.PacketPipeline;

/**
 * Editcore main class
 */
public class EditCore {

    public static final boolean DEBUG = true;
    private static EditCore INSTANCE;

    private final ECPlatform platform;

    private EditCore(ECPlatform platform) {
        this.platform = platform;
    }

    /**
     * @return Editcore main Instance
     */
    public static EditCore getInstance(){
        if(INSTANCE == null){
            ECLogger.error("Something tried to get editcore's instance before it was initialised.", new Throwable());
            return null;
        }
        return INSTANCE;
    }

    /**
     * Initialize editcore with a platform
     * @param platform
     */
    public static void init(ECPlatform platform){
        INSTANCE = new EditCore(platform);

        platform.getPinelineAdapter().register(new PacketPipeline());
    }

    public ECPlatform getPlatform() {
        return platform;
    }
}
